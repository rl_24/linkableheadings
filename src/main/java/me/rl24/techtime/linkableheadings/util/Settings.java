package me.rl24.techtime.linkableheadings.util;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import me.rl24.techtime.linkableheadings.impl.LinkableHeadingsComponent;

/**
 * Linkable Headings configuration class
 */
public class Settings {

    /**
     * Enum for use if the plugin is expanded and requires more configuration
     */
    private enum ConfigKey {
        ACTIVE_STATE
    }

    private static PluginSettings settings;

    /**
     * Get the plugin settings instance, newly created if not already existing
     * @return The plugin settings instance
     */
    private static PluginSettings getSettings() {
        if (settings == null)
            settings = LinkableHeadingsComponent.getInstance().getPluginSettingsFactory().createGlobalSettings();
        return settings;
    }

    /**
     * Get the settings key for a provided space
     * @param key       The space key
     * @param configKey The configuration key
     * @return The compiled settings key
     */
    private static String getSettingsKey(String key, ConfigKey configKey) {
        return String.format("%s.%s.%s", LinkableHeadingsComponent.getInstance().getName(), key, configKey.name());
    }

    /**
     * Set the active state of this plugin for the provided space to use
     * @param key         The space key
     * @param activeState The active state of the plugin
     */
    public static void setActiveState(String key, boolean activeState) {
        getSettings().put(getSettingsKey(key, ConfigKey.ACTIVE_STATE), String.valueOf(activeState));
    }

    /**
     * Get the active state of the plugin for the provided space
     * @param key The space key
     * @return The active state of the plugin
     */
    public static boolean getActiveState(String key) {
        Object activeState = getSettings().get(getSettingsKey(key, ConfigKey.ACTIVE_STATE));
        if (activeState == null) {
            setActiveState(key, true);
            return true;
        }
        return Boolean.parseBoolean(String.valueOf(activeState));
    }

}
