package me.rl24.techtime.linkableheadings.action;

import com.atlassian.confluence.spaces.actions.SpaceAdminAction;
import me.rl24.techtime.linkableheadings.util.Settings;

/**
 * Space tools configuration action
 */
public class ConfigAction extends SpaceAdminAction {

    public String activeState;

    /**
     * Config page load method
     * @return The page load type
     */
    public String doCreate() {
        if (activeState != null)
            Settings.setActiveState(getSpaceKey(), Boolean.parseBoolean(activeState));
        else
            activeState = String.valueOf(Settings.getActiveState(getSpaceKey()));
        return SUCCESS;
    }

}
