package me.rl24.techtime.linkableheadings.impl;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import me.rl24.techtime.linkableheadings.api.ILinkableHeadingsComponent;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Generated classes from 'atlas-create-confluence-plugin'
 */
@ExportAsService({ILinkableHeadingsComponent.class})
@Named("linkableHeadingsComponent")
public class LinkableHeadingsComponent implements ILinkableHeadingsComponent {

    /**
     * Create a singleton from the component for use in other classes
     */
    private static LinkableHeadingsComponent instance;
    public static LinkableHeadingsComponent getInstance() {
        return instance;
    }

    @ComponentImport
    private final ApplicationProperties applicationProperties;

    @ComponentImport
    private final PluginSettingsFactory pluginSettingsFactory;

    @Inject
    public LinkableHeadingsComponent(final ApplicationProperties applicationProperties, final PluginSettingsFactory pluginSettingsFactory) {
        instance = this;
        this.applicationProperties = applicationProperties;
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    public String getName() {
        if (applicationProperties != null)
            return "linkableHeadingsComponent:" + applicationProperties.getDisplayName();

        return "linkableHeadingsComponent";
    }

    public PluginSettingsFactory getPluginSettingsFactory() {
        return pluginSettingsFactory;
    }
}