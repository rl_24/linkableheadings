package me.rl24.techtime.linkableheadings.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.*;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import me.rl24.techtime.linkableheadings.util.Settings;

import java.util.Map;

/**
 * Confluence Linkable Headings Macro class
 */
public class LinkableHeadingsMacro implements Macro {

    /**
     * Execute the creation of the Macro
     * @param map               The Macro parameter map
     * @param s                 The body of the Macro
     * @param conversionContext The conversion context of the Macro
     * @return The renderable content for the Macro
     */
    @Override
    public String execute(Map<String, String> map, String s, ConversionContext conversionContext) {
        return (Settings.getActiveState(conversionContext.getSpaceKey())) ? VelocityUtils.getRenderedTemplate("templates/linkableheadings.vm", (Map) null) : "";
    }

    /**
     * Get the body type of the Macro
     * @return The body type of the Macro
     */
    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    /**
     * Get the output type of the Macro
     * @return The output type of the Macro
     */
    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

}
