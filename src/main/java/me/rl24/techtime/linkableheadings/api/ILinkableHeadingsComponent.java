package me.rl24.techtime.linkableheadings.api;

/**
 * Generated classes from 'atlas-create-confluence-plugin'
 */
public interface ILinkableHeadingsComponent {

    String getName();
}