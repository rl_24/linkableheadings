package it.me.rl24.techtime.linkableheadings;

import org.junit.Test;
import org.junit.runner.RunWith;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import me.rl24.techtime.linkableheadings.api.ILinkableHeadingsComponent;
import com.atlassian.sal.api.ApplicationProperties;

import static org.junit.Assert.assertEquals;

@RunWith(AtlassianPluginsTestRunner.class)
public class LinkableHeadingsComponentWiredTest {
    private final ApplicationProperties applicationProperties;
    private final ILinkableHeadingsComponent linkableHeadingsComponent;

    public LinkableHeadingsComponentWiredTest(ApplicationProperties applicationProperties, ILinkableHeadingsComponent linkableHeadingsComponent) {
        this.applicationProperties = applicationProperties;
        this.linkableHeadingsComponent = linkableHeadingsComponent;
    }

    @Test
    public void testMyName() {
        assertEquals("names do not match!", "linkableHeadingsComponent:" + applicationProperties.getDisplayName(), linkableHeadingsComponent.getName());
    }
}