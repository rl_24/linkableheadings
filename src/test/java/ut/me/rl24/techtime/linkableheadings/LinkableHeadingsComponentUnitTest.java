package ut.me.rl24.techtime.linkableheadings;

import org.junit.Test;
import me.rl24.techtime.linkableheadings.api.ILinkableHeadingsComponent;
import me.rl24.techtime.linkableheadings.impl.LinkableHeadingsComponent;

import static org.junit.Assert.assertEquals;

public class LinkableHeadingsComponentUnitTest {
    @Test
    public void testMyName() {
        ILinkableHeadingsComponent component = new LinkableHeadingsComponent(null, null);
        assertEquals("names do not match!", "linkableHeadingsComponent", component.getName());
    }
}